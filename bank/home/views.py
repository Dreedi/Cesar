from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import json

# Create your views here.
class LandingView(View):
	template = 'home/index.html'
	def get(self, request):
		context = {}
		return render(request, self.template, context)


class DemoView(View):

	def get(self, request):
		template = 'home/demo.html'
		context = {}
		return render(request, template, context)

	def post(self, request):
		template = 'home/resume.html'
		username = request.POST.get('username_twitter')
		emotions = requests.get('http://local-dreedi.com:8000/api/twitter/' + username)

		#Foursquare
		'''URL = 'http://local-dreedi.com:8000/'
		client = requests.session()
		client.get(URL)
		print dict(client.cookies)
		csrftoken = client.cookies['csrftoken']
		cookies = dict(client.cookies)
		headers = {'Accept': 'application/json',  "X-CSRFToken": csrftoken}
		places_id = requests.get('http://local-dreedi.com:8000/api/foursquare/socialauth')'''
		try:
			emotions = emotions.json()
		except:
			return redirect('/demo')
		context = {
			"emotions": emotions
		}
		return render(request, template, context)
		#return HttpResponse(places_id)