from django.conf.urls import patterns, url
from home import views

urlpatterns = patterns('',
    url(r'^$', views.LandingView.as_view(), name='index'),
    url(r'^demo/$', views.DemoView.as_view(), name='demo'),
)
