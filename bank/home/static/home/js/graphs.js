window.onload = function () {
var random = function (){return Math.round(Math.random()*100)};
CanvasJS.addColorSet("Result",
                [//colorSet Array
                "#E44C65",
                "rgba(255,255,255,0.05)"             
                ]);
var OpAdventure, OpArtistic, OpEmotions, OpImagination, OpIntelectual, OpLiberalism,
CoConstance, CoCautiousness, CoResponsability, CoOrganizational, CoPersistance, CoCompetence,
ExEnergy, ExAsertivity, ExCheerful, ExExitment, ExOutgoing, Exsociable,
AgAltruism, AgCooperation, AgModest, AgMorality, AgSympathy, AgTrust,
EmAnger, EmAnxiety, EmDepression, EmImmpoderation, EmSelf, EmVulnerability,
GAltruism, GAssertiveness, GCooperation, GCautiousness,
VGSelf_discipline, VGDutifulness, VGTrust,
BMelancholy, BEmotionality, BSusceptible_to_stress, BAdventurousness, BArtistic_interests, BProne_to_worry,
VBUncompromising, VBImmoderation, VBFiery,
VVBAuthority_challenging,
Score;

OpAdventure = document.getElementById('OpAdventure').value;
OpArtistic = document.getElementById('OpArtistic').value;
OpEmotions = document.getElementById('OpEmotions').value;
OpImagination = document.getElementById('OpImagination').value;
OpIntelectual = document.getElementById('OpIntelectual').value;
OpLiberalism = document.getElementById('OpLiberalism').value;

CoConstance = document.getElementById('CoConstance').value;
CoCautiousness = document.getElementById('CoCautiousness').value;
CoResponsability = document.getElementById('CoResponsability').value;
CoOrganizational = document.getElementById('CoOrganizational').value;
CoPersistance = document.getElementById('CoPersistance').value;
CoCompetence = document.getElementById('CoCompetence').value;

ExEnergy = document.getElementById('ExEnergy').value;
ExAsertivity = document.getElementById('ExAsertivity').value;
ExCheerful = document.getElementById('ExCheerful').value;
ExExitment = document.getElementById('ExExitment').value;
ExOutgoing = document.getElementById('ExOutgoing').value;
Exsociable = document.getElementById('Exsociable').value;

AgAltruism = document.getElementById('AgAltruism').value;
AgCooperation = document.getElementById('AgCooperation').value;
AgModest = document.getElementById('AgModest').value;
AgMorality = document.getElementById('AgMorality').value;
AgSympathy = document.getElementById('AgSympathy').value;
AgTrust = document.getElementById('AgTrust').value;

EmAnger = document.getElementById('EmAnger').value;
EmAnxiety = document.getElementById('EmAnxiety').value;
EmDepression = document.getElementById('EmDepression').value;
EmImmpoderation = document.getElementById('EmImmpoderation').value;
EmSelf = document.getElementById('EmSelf').value;
EmVulnerability = document.getElementById('EmVulnerability').value;

GAltruism = document.getElementById('GAltruism').value;
GAssertiveness = document.getElementById('GAssertiveness').value;
GCooperation = document.getElementById('GCooperation').value;
GCautiousness = document.getElementById('GCautiousness').value;

VGSelf_discipline = document.getElementById('VGSelf_discipline').value;
VGDutifulness = document.getElementById('VGDutifulness').value;
VGTrust = document.getElementById('VGTrust').value;

BMelancholy = 100-document.getElementById('BMelancholy').value;
BEmotionality = 100-document.getElementById('BEmotionality').value;
BSusceptible_to_stress = 100-document.getElementById('BSusceptible_to_stress').value;
BAdventurousness = 100-document.getElementById('BAdventurousness').value;
BArtistic_interests = 100-document.getElementById('BArtistic_interests').value;
BProne_to_worry = 100-document.getElementById('BProne_to_worry').value;


VBUncompromising = 100-document.getElementById('VBUncompromising').value;
VBImmoderation = 100-document.getElementById('VBImmoderation').value;
VBFiery = 100-document.getElementById('VBFiery').value;

VVBAuthority_challenging = document.getElementById('VVBAuthority_challenging').value+9;
VVBAuthority_challenging_show = (50+parseFloat(VVBAuthority_challenging));
NegVVBAuthority_challenging = (50-VVBAuthority_challenging);

Score = document.getElementById('Score').value+9;
NegScore = (100-Score);

var chart1 = new CanvasJS.Chart("chartContainer-1",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Apertura",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   {  y: OpAdventure , indexLabel: "Aventurero" },
   {  y: OpArtistic , indexLabel: "Artistico" },
   {  y: OpEmotions , indexLabel: "Emociones Profundas" },
   {  y: OpImagination , indexLabel: "Imaginativo" },
   {  y: OpIntelectual , indexLabel: "Intelectual" },
   {  y: OpLiberalism , indexLabel: "Liberalista" }
   ]
 }
 ]
});
var chart2 = new CanvasJS.Chart("chartContainer-2",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Conciencia",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: CoConstance, indexLabel: "Constancia" },
   {  y: CoCautiousness, indexLabel: "Cautela" },
   {  y: CoResponsability, indexLabel: "Responsabilidad" },
   {  y: CoOrganizational, indexLabel: "Organizacion" },
   {  y: CoPersistance, indexLabel: "Persistencia" },
   {  y: CoCompetence, indexLabel: "Competencia" }
   ]
 }
 ]
});
var chart3 = new CanvasJS.Chart("chartContainer-3",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Extrovertividad",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: ExEnergy, indexLabel: "Energia" },
   {  y: ExAsertivity, indexLabel: "Asertividad" },
   {  y: ExCheerful, indexLabel: "Positividad" },
   {  y: ExExitment, indexLabel: "Impaciencia" },
   {  y: ExOutgoing, indexLabel: "Calidez" },
   {  y: Exsociable, indexLabel: "Sociabilidad" }
   ]
 }
 ]
});
var chart4 = new CanvasJS.Chart("chartContainer-4",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Aceptacion",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   {  y: AgAltruism, indexLabel: "Altruista" },
   {  y: AgCooperation, indexLabel: "Cooperatividad" },
   {  y: AgModest, indexLabel: "Modestia" },
   {  y: AgMorality, indexLabel: "Moralidad" },
   {  y: AgSympathy, indexLabel: "Empatia" },
   {  y: AgTrust, indexLabel: "Confianza" }
   ]
 }
 ]
});
var chart5 = new CanvasJS.Chart("chartContainer-5",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Emociones",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: EmAnger, indexLabel: "Enojo" },
   {  y: EmAnxiety, indexLabel: "Ansiedad" },
   {  y: EmDepression, indexLabel: "Depresion" },
   {  y: EmImmpoderation, indexLabel: "Falta de moderacion" },
   {  y: EmSelf, indexLabel: "Autoconsiencia" },
   {  y: EmVulnerability, indexLabel: "Irritabilidad" }
   ]
 }
 ]
});
var chart6 = new CanvasJS.Chart("chartContainer-6",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Necesidades",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "spline",
   dataPoints: [
   {  y: random(), indexLabel: "Retos" },
   {  y: random(), indexLabel: "Acercamiento" },
   {  y: random(), indexLabel: "Curiosidad" },
   {  y: random(), indexLabel: "Emocion" },
   {  y: random(), indexLabel: "Armonia" },
   {  y: random(), indexLabel: "Libertad" },
   {  y: random(), indexLabel: "Amor" },
   {  y: random(), indexLabel: "Estabilidad" },
   {  y: random(), indexLabel: "Estructura" }
   ]
 }
 ]
});
var chart7 = new CanvasJS.Chart("chartContainer-7",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Bueno",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   {  y: GAltruism , indexLabel: "Altruista" },
   {  y: GAssertiveness , indexLabel: "Asertividad" },
   {  y: GCooperation , indexLabel: "Cooperatividad" },
   {  y: GCautiousness , indexLabel: "Cautela" }
   ]
 }
 ]
});
var chart8 = new CanvasJS.Chart("chartContainer-8",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Muy Bueno",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: VGSelf_discipline, indexLabel: "Disciplina" },
   {  y: VGDutifulness, indexLabel: "Deber con el trabajo" },
   {  y: VGTrust, indexLabel: "Confianza" }
   ]
 }
 ]
});
var chart9 = new CanvasJS.Chart("chartContainer-9",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Malo",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: BMelancholy, indexLabel: "Melancolia" },
   {  y: BEmotionality, indexLabel: "Emocionalidad" },
   {  y: BSusceptible_to_stress, indexLabel: "Irritabilidad" },
   {  y: BAdventurousness, indexLabel: "Aventurero" },
   {  y: BArtistic_interests, indexLabel: "Artistico" },
   {  y: BProne_to_worry, indexLabel: "Preocupabilidad" }
   ]
 }
 ]
});
var chart10 = new CanvasJS.Chart("chartContainer-10",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  title:{
    text: "Muy malo",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   { y: VBUncompromising, indexLabel: "Falta de compromiso" },
   {  y: VBImmoderation, indexLabel: "Falta de moderacion" },
   {  y: VBFiery, indexLabel: "Enojo" }
   ]
 }
 ]
});
var chart11 = new CanvasJS.Chart("chartContainer-11",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  colorSet: "Result",
  title:{
    text: "Muy muy malo",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   {  y: NegVVBAuthority_challenging, indexLabel: "Reta a la autoridad" },
   {  y: VVBAuthority_challenging_show, indexLabel: "" }
   ]
 }
 ]
});
var chart12 = new CanvasJS.Chart("chartContainer-12",
{
  backgroundColor: "rgba(252,252,252,0.03)",
  colorSet: "Result",
  title:{
    text: "Resultado final",
    fontColor:"white"
  },
  animationEnabled: true,
  animationDuration: 900,
  data: [
  {
   type: "doughnut",
   dataPoints: [
   {  y: Score, indexLabel: "Aprobacion" },
   {  y: NegScore, indexLabel: "" }
   ]
 }
 ]
});
chart1.render();
chart2.render();
chart3.render();
chart4.render();
chart5.render();
chart6.render();
chart7.render();
chart8.render();
chart9.render();
chart10.render();
chart11.render();
chart12.render();
};