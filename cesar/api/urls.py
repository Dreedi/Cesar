from django.conf.urls import patterns, url
from api import views

urlpatterns = patterns('',
    url(r'^post_user_personality/', 'api.views.PostUserPersonality', name='home'),
    url(r'^twitter/(?P<twitter_username>\w+)', 'api.views.getInfo', name='getInfo'),
    url(r'^foursquare/socialauth', 'api.views.foursquareAPI', name='foursquareAPI'),
    url(r'^redirect/foursquare', 'api.views.foursquareRedirect', name='foursquareRedirect'),
    url(r'^instagram/socialauth', 'api.views.instagramAPI', name='instagramAPI'),
    url(r'^redirect/instagram', 'api.views.instagramRedirect', name='instagramRedirect'),
)
