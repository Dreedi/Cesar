from django.core import serializers
from django.shortcuts import render
from CesarUtilities import watson
import json
from django.views.decorators.csrf import csrf_exempt
from twython import Twython
from django.http import HttpResponse, HttpResponseRedirect
import json
import requests
from decimal import *
import foursquare
from instagram.client import InstagramAPI
import sys

APP_KEY = 'ocCZEXuRUCVG0LmjSXHD7GW7E'
APP_SECRET = '3zibTSG8CGazit46b6G1219Iwc5ohcNXLLF3266PaMRQAjkV4P'

GOOD_PERSONALITY = ["Cautiousness", "Assertiveness", "Altruism", "Cooperation"]
VERY_GOOD_PERSONALITY = ["Dutifulness", "Orderliness", "Self-discipline", "Trust"]
BAD_PERSONALITY = ["Adventurousness", "Artistic interests", "Emotionality", "Prone to worry", "Melancholy", "Susceptible to stress"]
VERY_BAD_PERSONALITY = ["Uncompromising", "Fiery", "Immoderation"]
VERY_VERY_BAD_PERSONALITY = ["Authority-challenging"]

# Create your views here.
@csrf_exempt
def PostUserPersonality(request):
	params = {}
	if request.method == 'POST':
		user_description = request.POST.get('user_description')

		if user_description != '' and len(user_description.split(' ')) > 99:
			user_description = user_description.encode('ascii', 'ignore')
			watson_prediction = watson.getWatsonPrediction(user_description)
			json_response = json.dumps(watson_prediction, sort_keys=True, indent=4, separators=(',', ': '))
			return HttpResponse(json_response, content_type='application/json')

	template = 'PostUserPersonality.html'
	return render(request, template, params)


@csrf_exempt
def getInfo(request, twitter_username):
	user_twitter = get_user_tweets(twitter_username, request)
	user_description = {"user_description": user_twitter["text"]}
    # change to local-dreedi.com
	user_info = requests.post("http://local-dreedi.com:8000/api/post_user_personality/", data=user_description)
	try:
		user_info = user_info.json()
	except:
		return HttpResponse({}, content_type='application/json')
	json_response = {}
	json_response["info"] = user_info
	json_response["info"] = get_all_personalities(json_response)
	json_response["profile_image_url"] = user_twitter["profile_image_url"]
	final_percentage = get_final_percentages(json_response["info"]["delimited"])
	json_response["final_percentage"] = float(final_percentage)
	json_response["info"]["percentages"] = get_all_percentages(json_response["info"]["delimited"])
	json_response = json.dumps(json_response, sort_keys=True, indent=4, separators=(',', ': '))
	return HttpResponse(json_response, content_type='application/json')


def get_final_percentages(info):
	final_value = 0
	for emotion in info["good"]:
		final_value = final_value + emotion["percentage"] * 1.0
	for emotion in info["very_good"]:
		final_value = final_value + (emotion["percentage"] * 1.5)
	for emotion in info["bad"]:
		final_value = final_value + ((1 - emotion["percentage"]) * 1.0)
	for emotion in info["very_bad"]:
		final_value = final_value + ((1 - emotion["percentage"]) * 1.5)
	for emotion in info["very_very_bad"]:
		final_value = final_value + ((1 - emotion["percentage"]) * 1.8)
	final_percentage = Decimal(final_value * 100 / 22.3).quantize(Decimal('.01'), rounding=ROUND_DOWN)
	if not final_percentage + 10 >= 100:
		final_percentage += 10
	return final_percentage

def get_all_personalities(info):
	delimited_personalities = {"good": [], "very_good": [], "bad": [], "very_bad": [], "very_very_bad": []}
	all_personalities = {}
	for x in range(0,5):
		for personality in info["info"]["tree"]["children"][0]["children"][0]["children"][x]["children"]:
			personality = {"name": personality["name"], "percentage": personality["percentage"]}
			if personality["name"] in GOOD_PERSONALITY:
				delimited_personalities["good"].append(personality)

			elif personality["name"] in VERY_GOOD_PERSONALITY:
				delimited_personalities["very_good"].append(personality)

			elif personality["name"] in BAD_PERSONALITY:
				delimited_personalities["bad"].append(personality)

			elif personality["name"] in VERY_BAD_PERSONALITY:
				delimited_personalities["very_bad"].append(personality)

			elif personality["name"] in VERY_VERY_BAD_PERSONALITY:
				delimited_personalities["very_very_bad"].append(personality)

			personality["name"] = personality["name"].encode()
			personality["name"] = personality["name"].replace(" ", "_")
			personality["name"] = personality["name"].replace("-", "_")
			personality["name"] = personality["name"].decode()

			all_personalities[personality["name"]] = personality["percentage"] * 100
	return {"delimited": delimited_personalities, "all": all_personalities}

def get_all_percentages(info):
	all_personalities = {"good": {}, "very_good": {}, "bad": {}, "very_bad": {}, "very_very_bad": {}}

	for personality in info["bad"]:
		current_name = personality["name"]
		current_percentage = personality["percentage"]
		all_personalities["bad"][current_name] = (1 - current_percentage) * 100

	for personality in info["good"]:
		current_name = personality["name"]
		current_percentage = personality["percentage"]
		all_personalities["good"][current_name] = current_percentage * 100

	for personality in info["very_good"]:
		current_name = personality["name"]
		current_percentage = personality["percentage"]
		all_personalities["very_good"][current_name] = current_percentage * 100

	for personality in info["very_bad"]:
		current_name = personality["name"]
		current_percentage = personality["percentage"]
		all_personalities["very_bad"][current_name] = (1 - current_percentage) * 100

	for personality in info["very_very_bad"]:
		current_name = personality["name"]
		current_percentage = personality["percentage"]
		all_personalities["very_very_bad"][current_name] = (1 - current_percentage) * 100



	return all_personalities

@csrf_exempt
def get_user_tweets(twitter_username, request):
	twitter = authentication_version_2(request)
	username = twitter_username
	user_timeline = twitter.get_user_timeline(screen_name=username, count=200)
	all_tweets = {"text": ""}
	for tweet in user_timeline:
		all_tweets["text"] = all_tweets["text"] + ".\n" + tweet['text']
	return get_profile_picture(username, twitter, all_tweets)

@csrf_exempt
def authentication_version_2(request):
	twitter = Twython(APP_KEY, APP_SECRET, oauth_version=2)
	ACCESS_TOKEN = twitter.obtain_access_token()
	return Twython(APP_KEY, access_token=ACCESS_TOKEN)

@csrf_exempt
def go_to_social_auth(request):
	twitter = Twython(APP_KEY, APP_SECRET)
	auth = twitter.get_authentication_tokens(callback_url='http://local-dreedi.com:8000/twitter/twitterCallback/')
	global OAUTH_TOKEN
	OAUTH_TOKEN = auth['oauth_token']
	global OAUTH_TOKEN_SECRET
	OAUTH_TOKEN_SECRET = auth['oauth_token_secret']
	return HttpResponseRedirect(auth['auth_url'])

@csrf_exempt
def handling_twitter_callback(request):
	oauth_verifier = request.GET['oauth_verifier']
	twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
	final_step = twitter.get_authorized_tokens(oauth_verifier)
	user_timeline = twitter.get_home_timeline()
	return HttpResponse('k')

@csrf_exempt
def get_profile_picture(twitter_username, twitter, user_twitter):
	user = twitter.show_user(screen_name=twitter_username)
	user_twitter["profile_image_url"] = user['profile_image_url']
	return user_twitter


def foursquareAPI(request):
	client = get_foursquare_client()
	return HttpResponseRedirect(client.oauth.auth_url())


def foursquareRedirect(request):
	client = get_foursquare_client()
	print "Hola"
	access_token = client.oauth.get_token(request.GET['code'].encode("utf-8"))
	client.set_access_token(access_token)
	venues = client.users.venuehistory()["venues"]["items"]
	categories_id = []
	for venue in venues:
		for category in venue["venue"]["categories"]:
			categories_id.append(category["id"])
	categories_id = json.dumps(categories_id, sort_keys=True, indent=4, separators=(',', ': '))

	return HttpResponse(categories_id, content_type='application/json')

def get_foursquare_client():
	return foursquare.Foursquare(client_id='PTC40VYSFRSDOLXZHBBMOHU4YWCI14UIZBDGR34FZX4ULTJ5', client_secret='2VUYT3UEAQPFPMGPR3512BQBMRFMD5JEVRM0U2WNLWEIV3MC', redirect_uri='http://local-dreedi.com:8000/api/redirect/foursquare')
	
def instagramAPI(request):
	try:
	    import __builtin__
	    input = getattr(__builtin__, 'raw_input')
	except (ImportError, AttributeError):
	    pass

	client_id = 'c1ab897abe80436b908a390c2bd18f80'
	client_secret = 'f423ff4a4fb444ac946bceab5d35b763'
	redirect_uri = 'http://local-dreedi.com:8000/api/redirect/instagram/socialauth'
	scope = ["basic"]

	api = InstagramAPI(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri)
	redirect_uri = api.get_authorize_login_url(scope = scope)

	return  HttpResponseRedirect(redirect_uri)

	code = (str(input("Paste in code in query string after redirect: ").strip()))

	access_token = api.exchange_code_for_access_token(code)
	print ("access token: " )
	print (access_token)

def inflateInstagramAPI():
	return InstagramAPI(client_id='c1ab897abe80436b908a390c2bd18f80', client_secret='f423ff4a4fb444ac946bceab5d35b763', redirect_uri='http://local-dreedi.com:8000/api/redirect/instagram/socialauth')

def instagramRedirect(request):
	api = inflateInstagramAPI()
	access_token = api.exchange_code_for_access_token(request.GET['code'])
	location = api.location(location_id=access_token[1]['id'])
	print location
	return HttpResponse(location)