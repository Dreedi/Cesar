import requests
import json

def getWatsonPrediction(text):
	headers = {"Authorization": "Basic ZTI5ZTBjZTgtMmE0YS00ODNkLWIwNTgtZTNiYWI5Njg1MGU4OjE3d01pYlRWUDVaNg",
			  "Content-Type": "text/plain"}

	r = requests.post("https://gateway.watsonplatform.net/personality-insights/api/v2/profile", data=text, headers=headers)

	watson_result = r.json()

	return watson_result
